## Création d'une liaison 1-N dans Domain

Voici un exemple d'une relation Équipe-Personne

```cs
public class Equipe : Entity
{
    // Atttibuts de l'équipe (nom, logo, ...)
    public list<Personne> Membres { get; set; }
    
}

public class Personne : Entity
{
    // Atttibuts de la personne (nom, prénom, ddn, ...)
    public Equipe Equipe { get; set; }
    
}
```

https://learn.microsoft.com/en-us/ef/core/modeling/relationships

## Création d'une liaison N-N dans Domain

La relation N-N peut être vu comme deux relations 1-N avec la création d'une entité comme table intermédiaire entre les deux entités.

Voici un exemple d'une relation Cours-Personne.

```cs
public class Cours : Entity
{
    // Atttibuts de cours (nom, numéro, local, horaire ...)
    public list<Personne> Etudiants { get; set; }
    
}

public class Personne : Entity
{
    // Atttibuts de la personne (nom, prénom, ddn, ...)
    public list<Cours> Cours { get; set; }
    
}

public class CoursPersonne : Entity
{
    public Personne Personne { get; set; }
    public Cours Cours { get; set; }
}
```

https://learn.microsoft.com/en-us/ef/core/modeling/relationships/many-to-many