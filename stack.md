## StackTechno

- PostgresSQL
- .NET 6
- C#
- Vue 3

## Architecture

- [REPR desing pattern](https://deviq.com/design-patterns/repr-design-pattern) 
- Vertical slices
- 4 projets dans la solution
  - Infrastructure
  - Domain
  - Application
  - Web

![Architecture stack SPK](architectureStackSPK.PNG "Architecture stack")

[Retour à l'accueil](/README.md)