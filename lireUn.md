## Création d'un lire un dans la couche Web

Étapes : DTOWeb -> Controller -> Type -> Store -> Vue/Component

1. Au besoin, ajouter un attribut id dans votre entité et faire les migrations.
2. (Si c'est votre première entité) Créer un répertoire DTOWeb à la racine du projet Web
3. Dans Web\DTOWeb, créer une classe nommée ReadEntiteDTOWeb. Cette classe contiendra un DTO pour chaque useCase. Ces DTOs peuvent aussi être vu comme une requête pour les informations de votre entité que vous avez besoin au niveau de la couche Web. Ils seront souvent identique au EntiteDTOApp, mais c'est possible qu'un DTOWeb n'utilise pas tous les éléments du DTOApp. 

```cs
    public class ReadEntiteDTOWeb
    {
        public string Attribut1 { get; set; }
        public string Attribut2 { get; set; }
        public double Attribut3 { get; set; }
        public DateTime Attribut4 { get; set; }
        //etc.
    }
```

4. **(S'il n'est pas déjà créé)** Dans Web\Controller, ajouter un controller nommé EntiteController qui hérite de ControllerBase. Ce controller gérera les routes pour appeler la couche applicative. Voici un exemple avec le get

```cs
[ApiController]
[Route("entite")]
public class EntiteController : ControllerBase
{
    private readonly IGestionEntite _gestionEntites;

    public PartnerController(IGestionEntite gestionEntites)
    {
        _gestionEntites = gestionEntites;
    }

    [HttpGet("{idNomDuParam}")]
    public IActionResult Get(int idNomDuParam)
    {
        var entite = _gestionEntite.GetEntite(id);
        var entiteRetour = new ReadEntiteDTOWeb
        {
          //À gauche ce sont les attributs de EntiteDTOWeb et à droite ce sont les attributs du GetEntite de la couche Applicatio
            attribut1 = entite.attribut1,
            attribut2 = entite.attribut2,
            attribut3 = entite.attribut3,
            attribut4 = entite.attribut4 
        };


        return Ok(entiteRetour);
    }

}
```

5. **(S'il n'est pas déjà créé)** Dans Web\vue-app\src\types, créer un fichier entite.ts qui définira ce qui doit être visible dans le UI. Vous pouvez définir plusieurs interfaces selon chacun de vos useCase.

```cs

export interface ReadEntiteType extends ReadAllEntiteType {
    attribut1 : string,
    attribut2 : string,
    attribut3 : double,
    attribut4 : datetime,
}
```

6. **(S'il n'est pas déjà créé)** Dans Web\vue-app\src\stores, créer un fichier entites.ts qui définira vos actions du UI. 

```cs
import { defineStore } from 'pinia'
import { ReadAllEntiteType } from '../types/entite'

export const useEntites = defineStore('Entite', {

    actions: { // methods

        async GetEntite(id: number): Promise<ReadEntiteType> {
           
            const res = await fetch('/entite/'+id, {
                headers: {
                    'Content-Type': 'application/json'
                    }
            })
            const result = await res.json()
            return result;
        },

    },
})
```

7. Dans Web\vue-app\src\views, créer votre vue Entites.Vue. Cette vue contiendra le html et le script. Pour le html, utilisé la balise Suspense pour bien gérer les promises. Pour le script n'oubliez pas les options de langage et le setup. 

```vue
<template>

    <Suspense>
        <!-- Ce qui sera affiché quand toutes les promises seront résolues -->
        <template #default>
            <div>
                    <p>{{ entite.attribut1 }} </p>
                    <p>{{ entite.attribut2 }} </p>
                    <p>{{ entite.attribut3 }} </p>
                    <p>{{ entite.attribut4 }} </p>

                </div>

        </template>
        <!-- Ce qui sera affiché avant que toutes les promises seront résolues -->
        <template #fallback>
            <progress class="progress is-primary is-small" />
        </template>
    </Suspense>
</template>

<script lang="ts" setup>

    import { useEntites } from "../stores/entites";
    import { onBeforeMount, ref } from "vue";
    import { useRoute } from "vue-router";

    const entitesStore = useEntites();
    const entite = ref({});

    const route = useRoute();
    const id = route.params.id;

    onBeforeMount(async () => {
        entite.value = await entitesStore.GetEntite(id.toString());
    });

</script>
```
8. Dans Web\vue-app\src\router.ts, ajoutez une route vers votre vue.

```cs
export const router = createRouter({
    history: createWebHistory(),
    routes: [

        ...

        {
            path: '/entites/:id',
            component: Entites
        },
    ]
})
```

9. Pour lier vers la route dans votre app Vue, il est recommandé d'utiliser un routerlink.

```cs
<RouterLink to="/entites" class="button">Caption du bouton</RouterLink>
```

[Retour à l'accueil](/README.md)