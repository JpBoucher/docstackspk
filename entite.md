## Création d'une entité dans Domain
  1. Créer un dossier au nom de votre entité dans Domain\Entities
  2. Ajouter une classe .cs au nom de votre entité
  3. Faire hériter la classe de Entity
  4. Ajouter les attributs que vous avez besoin ainsi :
  5. (Si nécessaire) Ajouter un champ unique que vous utiliserez pour le routing dans votre couche web (par exemple : monsite.com/monentite/monchampunique)

```cs
public class NomDeMonEntite : Entity
{
    public string Attribut1 { get; set; }
    public string Attribut2 { get; set; }
    public double Attribut3 { get; set; }
    public DateTime Attribut4 { get; set; }
    //etc.
}
```

  5. Dans Infrastructure\Persistence\NomDeAppDeContext.cs, ajouter un dbSet public pour votre entité

```cs
public DbSet<NomDeMonEntite> NomDeMonEntite { get; set; }
```
  6. (Si c'est votre première entité) Si Infrastructure\Persistence\NomDeAppDeContext.cs n'a pas de méthode SaveChangesAsync, ajouter la méthode suivante dans la classe :
   
```cs
public Task<int> SaveChangesAsync()
{
    return base.SaveChangesAsync();
}
```

  7. Dans Application\Interfaces\INomDeAppDeContext.cs, ajouter un dbSet pour votre entité 
```cs
    DbSet<NomDeMonEntite> NomDeMonEntite { get; set; }
```
  8. (Optionnel) Si Application\Interfaces\INomDeAppDeContext.cs, n'a pas de méthode SaveChangesAsync, ajouter la méthode suivante dans la classe :
   
```cs
    Task<int> SaveChangesAsync();
```
9. [Exécuter la commande d'ajout de migration en donnant un nom significatif à la migration](#Migrations).
10. [Exécuter la commande de mise à jour de la bd](#Migrations).

[Retour à la table des matières](#Contenu)

## Ajout de l'entité dans la couche applicative (Projet Application)

1. (Si c'est votre première entité) Dans Application, créer un répertoire Services
2. Dans Application\Services, créer un répertoire au nom de votre entité
3. Dans Application\Services\VotreEntité, créer un répertoire DTOApp
4. Dans Application\Services\VotreEntité\DTOApp, créer une classe nommée VotreEntitéDTOApp (par exemple EtudiantDTOApp). Cette classe contiendra les informations de votre entité que vous avez besoin de passer de la couche Domain à la couche Web pour l'action spécifiée. Il est possible que certains éléments de votre entité dans Domain ne se trouve pas dans le DTOApp (par exemple, id ou dateCreation)

```cs
    public class NomDeVotreEntiteDTOApp
    {
        public string Attribut1 { get; set; }
        public string Attribut2 { get; set; }
        public double Attribut3 { get; set; }
        public DateTime Attribut4 { get; set; }
        //etc.
    }
```
5. Dans Application\Services\VotreEntite ajouter une interface IGestionVotreEntité. Cette interface exposera à la couche Web les actions que vous voulez permettre (dans l'exemple on permet un lire, un lire tous et un creer)

```cs
    public interface IGestionEntite
    {
        Task CreateEntite(NomDeVotreEntiteDTOApp NomDeVotreEntiteDTOApp);
        List<Entite> GetEntites();
        Entite GetEntite(Guid id);
    }
```
6. Dans le répertoire Application\Services\VotreEntite ajouter une classe GestionEntite (c’est ce que Rose appelait un service et c’est ce qui fait le lien entre le front et le back). Cette classe implémente l’interface créée en #5 et contient le contexte de la db.

```cs
public class GestionEntite : IGestionEntite
{

    private INomDeAppDbContext DbContext { get; }

    public GestionEntite(INomDeAppDbContext dbContext)
    {
        DbContext = dbContext;
    }

    public async Task CreerEntite(NomDeVotreEntiteDTOApp NomDeVotreEntiteDTOApp)
    {
        // Faire le lien entre le DTOApp et la classe dans Domain/Entities
        DbContext.Entite.Add(new Entite
        {
            Attribut1 = NomDeVotreEntiteDTOApp.Attribut1,
            Attribut2 = NomDeVotreEntiteDTOApp.Attribut2,
            Attribut3 = NomDeVotreEntiteDTOApp.Attribut3,
            Attribut4 = NomDeVotreEntiteDTOApp.Attribut4
        });
        await DbContext.SaveChangesAsync();
    }

    public Entite GetEntite(Guid id)
    {
        return DbContext.Entite.FirstOrDefault(x => x.Id == id);
    }

    public List<Entite> GetEntites()
    {
        return DbContext.Entite.ToList();
    }
}
```
7. Dans Application\ConfigureServices.cs rendre votre service accessible dans la solution avec la ligne suivante.
```cs
    services.AddScoped<IGestionEntite, GestionEntite>();
```

8. (Si c'est votre première entité) Dans Infrastructure\ConfigureServices.cs rendre le service de Bd accessible dans la solution avec la ligne suivante. 
   **Deux mises en garde pour cette instruction:** 
   1. Placer la ligne plus bas que le services.AddDbContext
   2. Assurez vous que le type du AddScoped est l'interface et que le type du GetRequiredService est le dbContext.

```cs
    services.AddScoped<INomDeAppDbContext>(provider => provider.GetRequiredService<NomDeAppDbContext>());
```

[Retour à l'accueil](/README.md)