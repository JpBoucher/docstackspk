## Glossaire

### DTO
DTO est l'abbréviation pour Data Transfer Object. Les DTOs sont principalement utilisé dans les systèmes à N-couches. L'architecture du projet actuel peut être considérée comme une architecture en 4 couches (Domain, Application, Web et Infrastructure). Les DTOs sont utilisés pour encapsuler des données et les faire transférer une couche du projet en un seul appel.

Les DTOs servent aussi à réduire les dépendances entre les couches. Ainsi, on peut facilement changer une des couches sans qu'il y ait trop de conséquences sur les autres couches. Par contre, les DTOs compliquent l'architecture, car on se retrouve avec beaucoup d'objets avec des structures similaires.

Dans notre cas, nous utiliserons principalement les DTOs à deux endroits :
1. Les DTO dans le projet Application, que nous nommerons DTOApp pour éviter la confusion. Les DTOApp seront la modélisation de nos entités dans la couche applicative.
2. Les DTO dans le projet Web, que nous nommerons DTOWeb pour éviter la confusion. Les DTOWeb seront la modélisation de nos entités dans la couche Web.

### Promise
Les promise en js sont des opérations asynchrones. En utilisant la balise suspense, nous sommes en mesure de charger la page en attendant que la promise asynchrone soient complétée.

[Retour à l'accueil](/README.md)