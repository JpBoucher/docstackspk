## Création d'un créer dans la couche Web

Étapes : DTOWeb -> Controller -> Type -> Store -> Vue/Component

1. (Si c'est votre première entité) Créer un répertoire DTOWeb à la racine du projet Web
2. Dans Web\DTOWeb, créer une classe nommée ActionEntiteDTOWeb. Cette classe contiendra un DTO pour chaque useCase. Ces DTOs peuvent aussi être vu comme une requête pour les informations de votre entité que vous avez besoin au niveau de la couche Web. Ils seront souvent identique au EntiteDTOApp, mais c'est possible qu'un DTOWeb n'utilise pas tous les éléments du DTOApp. 

```cs
    public class CreerEntiteDTOWeb
    {
        public string Attribut1 { get; set; }
        public string Attribut2 { get; set; }
        public double Attribut3 { get; set; }
        public DateTime Attribut4 { get; set; }
        //etc.
    }
```

3. **(S'il n'est pas déjà créé)** Dans Web\Controller, ajouter un controller nommé EntiteController qui hérite de ControllerBase. Ce controller gérera les routes pour appeler la couche applicative. Voici un exemple avec un create

```cs
[ApiController]
[Route("entites")]
public class EntiteController : ControllerBase
{
    private readonly IGestionEntite _gestionEntites;

    public PartnerController(IGestionEntite gestionEntites)
    {
        _gestionEntites = gestionEntites;
    }

    
    [HttpPost]
    [Route("ajout")]
    public async Task<IActionResult> Post (JsonDocument entiteRequete)
    {
        try
        {
            NomDeVotreEntiteDTOApp nouveauEntite = new NomDeVotreEntiteDTOApp
            {
                 //À gauche ce sont les attributs de NomDeVotreEntiteDTOApp et à droite ce sont les attributs du json reçu en paramètre
                attribut1 = creerBeneReq.RootElement.GetProperty("attribut1").GetString(),
                attribut2 = creerBeneReq.RootElement.GetProperty("attribut2").GetString(),
                attribut3 = creerBeneReq.RootElement.GetProperty("attribut3").GetString(),
                attribut4 = creerBeneReq.RootElement.GetProperty("attribut4").GetString()
            };
            await _gestionBenevole.CreateBenevole(newBenevole);
        }
        catch (Exception exeption)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, exeption.Message);
        }
        return StatusCode(StatusCodes.Status201Created);

    }

}
```

4. **(S'il n'est pas déjà créé)** Dans Web\vue-app\src\types, créer un fichier entite.ts qui définira ce qui doit être visible dans le UI. Vous pouvez définir plusieurs interfaces selon chacun de vos useCase.

```cs
export interface CreateEntiteType {
    attribut1 : string,
    attribut2 : string,
    attribut3 : double,
    attribut4 : datetime,
}

```

5. **(S'il n'est pas déjà créé)** Dans Web\vue-app\src\stores, créer un fichier entites.ts qui définira vos actions du UI. 

```cs
import { defineStore } from 'pinia'
import { CreateEntiteType } from '../types/entite'

export const useEntites = defineStore('Entite', {

    actions: { // methods

       async create(nouveauEntite: CreateEntiteType) {

            const body = JSON.stringify(nouveauEntite)
            await window.fetch("/entites/ajout", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body
            })
        }
 
    },
})
```

1. Dans Web\vue-app\src\views, créer votre vue Entites.Vue. Cette vue contiendra le html et le script. Pour le script n'oubliez pas les options de langage et le setup. 

```vue
<template>

   <form>
        <!-- faire votre formulaire d'ajout ici-->
        <!-- utiliser des inputs avec les attribut ref-->
        <input v-model="attribut1Form" placeholder="" type="text">
        <button @click="clickDuBouton">Ajouter nouveau entité</button>
        

    </form>
</template>

<script lang="ts" setup>
    import { ref } from "vue";
    import { useEntites } from "../stores/entites";
    import { CreateEntiteType } from '../types/entite'
    import { useRouter } from "vue-router";

    const entitesStore = useEntites();
    const router = useRouter();
    const attribut1Form = ref('');
    const attribut2Form = ref('');
    const attribut3Form = ref('');
    const attribut4Form = ref('');

    function clickDuBouton() {
        const nouveauEntite: CreateEntiteType = {
            attribut1: attribut1Form.value,
            attribut2: attribut2Form.value,
            attribut3: attribut3Form.value,
            attribut4: attribut4Form.value,
        };

        entitesStore.create(nouveauEntite);
        router.push("/");
    }

</script>
```
7. Dans Web\vue-app\src\router.ts, ajoutez une route vers votre vue.

```cs
export const router = createRouter({
    history: createWebHistory(),
    routes: [

        ...

        {
            path: '/entites',
            component: Entites
        },
    ]
})
```

8. Pour lier vers la route dans votre app Vue, il est recommandé d'utiliser un routerlink.

```cs
<RouterLink to="/entites" class="button">Caption du bouton</RouterLink>
```

[Retour à l'accueil](/README.md)