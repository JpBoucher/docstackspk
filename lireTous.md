## Création d'un lire tous dans la couche Web

Étapes : DTOWeb -> Controller -> Type -> Store -> Vue/Component

1. (Si c'est votre première entité) Créer un répertoire DTOWeb à la racine du projet Web
2. Dans Web\DTOWeb, créer une classe nommée ActionEntiteDTOWeb. Cette classe contiendra un DTO pour chaque useCase. Ces DTOs peuvent aussi être vu comme une requête pour les informations de votre entité que vous avez besoin au niveau de la couche Web. Ils seront souvent identique au EntiteDTOApp, mais c'est possible qu'un DTOWeb n'utilise pas tous les éléments du DTOApp. 

```cs
    public class ReadAllEntiteDTOWeb
    {
        public string Attribut1 { get; set; }
        public string Attribut2 { get; set; }
        //etc.
    }
```

3. **(S'il n'est pas déjà créé)** Dans Web\Controller, ajouter un controller nommé EntiteController qui hérite de ControllerBase. Ce controller gérera les routes pour appeler la couche applicative. Voici un exemple avec un getAll

```cs
[ApiController]
[Route("getEntites")]
public class EntiteController : ControllerBase
{
    private readonly IGestionEntite _gestionEntites;

    public PartnerController(IGestionEntite gestionEntites)
    {
        _gestionEntites = gestionEntites;
    }

    [HttpGet]
    public IActionResult GetAll()
    {
         var entites = _gestionEntites.GetEntites().Select(entite => new ReadAllEntiteDTOWeb
        {
          //À gauche ce sont les attributs de EntiteDTOWeb et à droite ce sont les attributs du GetEntites de la couche Application
            attribut1 = entite.attribut1,
            attribut2 = entite.attribut2
        });

        return Ok(entites);

    }
}
```

4. **(S'il n'est pas déjà créé)** Dans Web\vue-app\src\types, créer un fichier entite.ts qui définira ce qui doit être visible dans le UI. Vous pouvez définir plusieurs interfaces selon chacun de vos useCase.

```cs
export interface ReadAllEntiteType {
    attribut1 : string,
    attribut2 : string,
}

```

5. **(S'il n'est pas déjà créé)** Dans Web\vue-app\src\stores, créer un fichier entites.ts qui définira vos actions du UI. 

```cs
import { defineStore } from 'pinia'
import { ReadAllEntiteType } from '../types/entite'

export const useEntites = defineStore('Entite', {

    actions: { // methods

        async GetEntites(): Promise<ReadAllEntiteType[]> {
            const res = await fetch("getEntites", {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            const result = await res.json()
            return result.map((p) => ({
                // À gauche ce sont les attributs du type défini en #4 et à droite ce sera ce que le GetAll du controller retournera
                attribut1: p.attribut1,
                attribut2: p.attribut2

            }) as ReadAllEntiteType);
        }

 
    },
})
```

6. Dans Web\vue-app\src\views, créer votre vue Entites.Vue. Cette vue contiendra le html et le script. Pour le html, utilisé la balise Suspense pour bien gérer les promises. Pour le script n'oubliez pas les options de langage et le setup. 

```vue
<template>

    <Suspense>
        <!-- Ce qui sera affiché quand toutes les promises seront résolues -->
        <template #default>
             <v-table>
            <thead>
                <tr>
                    <th class="text-left">
                        Attribut1
                    </th>
                    <th class="text-left">
                        Attribut2
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="p in entites"
                    :key="p.attribut1">
                    <td>{{ p.attribut1 }}</td>
                    <td>{{ p.attribut2 }}</td>
                </tr>
            </tbody>
        </v-table>
        </template>
        <!-- Ce qui sera affiché avant que toutes les promises seront résolues -->
        <template #fallback>
            <progress class="progress is-primary is-small" />
        </template>
    </Suspense>
</template>

<script lang="ts" setup>
    import { onBeforeMount, ref } from "vue";
    import { useEntites } from "../stores/entites";
    import { ReadAllEntiteType } from '../types/entite'
    const entitesStore = useEntites();

    const lesEntites = ref<ReadAllEntiteType[]>([])
        onBeforeMount(async () => {
        lesEntites.value = await useEntites().GetEntites()
    })

</script>
```
7. Dans Web\vue-app\src\router.ts, ajoutez une route vers votre vue.

```cs
export const router = createRouter({
    history: createWebHistory(),
    routes: [

        ...

        {
            path: '/entites',
            component: Entites
        },
    ]
})
```

8. Pour lier vers la route dans votre app Vue, il est recommandé d'utiliser un routerlink.

```cs
<RouterLink to="/entites" class="button">Caption du bouton</RouterLink>
```

[Retour à l'accueil](/README.md)