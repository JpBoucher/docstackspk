## Déploiement
1. Merger sur la branche main distante
2. Attendre 5-10 min le temps que Appveyor redéploie
3. Aller sur Gitlab pour voir si le pipeline à réussi (le crochet vert ou le x rouge à droite du nom de votre projet)
4. Si le pipeline a failed, consulter [Appveyor](https://ci.appveyor.com/) avec les credentials que Rose vous a envoyé

## Debugging

### BD

1. Drop la bd dans PgAdmin et refaire la commande dbUpdate.
2. Si #1 ne fonctionne pas : 
   1. Supprimer toutes les migrations
   2. Supprimer le contenu du snapshot
   3. Faire la commande addMigration
   4. Faire la commande dbUpdate

### Backend
1. Placer les points d'arrêt
2. Lancer le frontend
3. Lancer l'application directement par Visual Studio en mode débogage avec le projet Web comme projet de démarrage et IIS Express comme serveur Web
![Debug backend](debugBackend.png "Debug backend")
1. Attendre que l'ouverture de la page Web dans le navigateur
2. Noter le port choisi par IIS Express
3. Faire des requêtes avec [Postman](https://www.postman.com/) afin de ne pas passer par le frontend

### Frontend
1. Avec des Console.Log pi la console du navigateur comme dans l'bon vieux temps 

### Deploiement
1. En console, aller dans ./src/Web/vue-app
2. Faire la commande npm run build
3. Corriger toute les erreurs

### Azure
À venir

[Retour à l'accueil](/README.md)