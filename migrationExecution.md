## Migrations

From the Infrastructure assembly:

```bash
$ cd .\src\Infrastructure

$ dotnet ef migrations add {MigrationName} --project Infrastructure.csproj --startup-project ../Web/Web.csproj

$ dotnet ef database update --project Infrastructure.csproj --startup-project ../Web/Web.csproj
```

## RunFrontEnd
```bash
# install dependencies
$ cd .\src\Web\vue-app
$ npm install

# run
$ npm run dev
```

## RunBackEnd
```bash
$ cd .\src\Web
$ dotnet watch run
```

[Retour à l'accueil](/README.md)
