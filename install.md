## Installation

- Install .NET Core 6.0.403
- Restore nuget package
- Install [pgAdmin](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads) for PostgreSQL database
- Open pgAdmin
- Create dev user with rights to create tables : 
    - Right click on Login/Group Roles, Create, then : 
      - General : Name = dev.
      - Definition : Password = dev.
      - Privileges : Can login & superuser.
- Install nvm 
- Install node 16.13.2
    ```bash
    $ nvm install 16.13.2
    $ nvm use 16.13.2
    ```

[Retour à l'accueil](/README.md)